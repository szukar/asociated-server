import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class Client {
    public static void main(String[] args) {
        try (Socket socket = new Socket("localhost", 8080)) {
            Map<String, String> map = new HashMap<>();
            map.put("key1", "val1");
            map.put("key2", "val2");
            String string = map.toString();
            if (string.length() >= 1024) {
                throw new Exception("Map too long");
            }
            socket.getOutputStream().write(string.getBytes());
            byte[] message = socket.getInputStream().readAllBytes();
            System.out.println(new String(message));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
