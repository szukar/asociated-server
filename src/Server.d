import tango.io.Console; //IO

import tango.net.device.Socket;

import tango.core.Thread;

import tango.core.Exception;


void main()
{
    ServerSocket server = new ServerSocket(8080);
    Cout("Server starting").newline;

    while(true) {
        try {
        Socket client = server.accept();
        Cout("New connection").newline;

        char[1024] buffer;
        int size = client.read(buffer);
        for (int i = 0; i < buffer.length; i++) {
            if (buffer[i] == '=') {
                buffer[i] = ':';
            }
        }
        client.write(buffer[0 .. size]);

        client.shutdown();
        client.detach();

        Thread thread = new Thread(
        {
            char[1024] buffer;
        int size = client.read(buffer);
        for (int i = 0; i < buffer.length; i++) {
            if (buffer[i] == '=') {
                buffer[i] = ':';
            }
        }
        client.write(buffer[0 .. size]);

        client.shutdown();
        client.detach();

        });
        thread.start();
        } catch (Exception e) {
            Cout("Exception!").newline;
        }
    }
}